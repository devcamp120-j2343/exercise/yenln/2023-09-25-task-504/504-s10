import { Person } from "./person.js";
import { Employee } from "./employee.js";

var person1 = new Person("John", 50, "male");
console.log(person1.getPersonInfo());
console.log(person1 instanceof Person);

var employee1 = new Employee("Michael", 15, "male", "John", 3000000, "staff");
console.log(employee1.getEmployeeInfo());
console.log(employee1.getSalary());
employee1.setSalary()
console.log(employee1.getSalary());
console.log(employee1.getPosition());
