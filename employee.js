import { Person } from "./person.js";

class Employee extends Person{
    employer;
    salary;
    position;
    constructor(personName, personAge, gender, employer, salary, position){
        super(personName, personAge, gender)
        this.employer = employer;
        this.salary = salary ; 
        this.position = position;
    }
    getEmployeeInfo(){
        console.log(this.personName);
        console.log(this.personAge);
        console.log(this.gender);
    }
    getSalary(){
        return this.salary;
    }
    setSalary(){
        this.salary = 10000000;
    }
    setPosition(){
        this.position = "manager";
    }
    getPosition(){
        console.log(this.position);
    }
}

export { Employee}