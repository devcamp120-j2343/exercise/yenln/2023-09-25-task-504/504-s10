import { Person } from "./person.js";

class Student extends Person{
    standard;
    collegeName;
    grade;

    constructor(personName, personAge, gender, standard, collegeName, grade){
        super(personName, personAge, gender);
        this.standard = standard;
        this.collegeName = collegeName;
        this.grade = grade;
    }

    getstudentInfo(){
        console.log(this.personName);
        console.log(this.personAge);
        console.log(this.gender);
        console.log(this.standard);
        console.log(this.grade);
    }
    getGrade(){
        console.log(`This grade ${this.grade}`)
    }
    setGrade(){
        this.grade = 5;
        console.log(`New grade ${this.grade}`)
    }
}

export{ Student}