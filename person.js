class Person {
    personName;
    personAge;
    gender;

    constructor (personName, personAge, gender){
        this.personName = personName;
        this.personAge = personAge;
        this.gender = gender;
    }

    getPersonInfo(){
        console.log(this.personName);
        console.log(this.personAge);
        console.log(this.gender);
    }
}

export {Person}