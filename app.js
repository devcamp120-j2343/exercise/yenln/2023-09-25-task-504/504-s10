import { Person } from "./person.js";
import { Student } from "./student.js";
var person1 = new Person("Hanni", 18, "female")

console.log(person1 instanceof Person);

var student1 = new Student("Chung", 19, "male", "premium", "Harvard", 9);
console.log(student1.getstudentInfo());
console.log(student1.getGrade())
console.log(student1.setGrade());